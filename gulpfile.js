const gulp = require('gulp');
const concat = require('gulp-concat');
const terser = require('gulp-terser');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const { src, dest, parallel, series, watch } = require('gulp');

function copyHtml() {
    return src('./src/*.html').pipe(gulp.dest('dist'));
}

function jsTask() {
    return src('./src/js/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('all.js'))
        .pipe(terser())
        .pipe(sourcemaps.write('.'))
        .pipe(dest('dist/assets/js'));
}

function cssTask() {
    return src('./src/css/*.css')
        .pipe(sourcemaps.init())
        .pipe(concat('style.css'))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(sourcemaps.write('.'))
        .pipe(dest('dist/assets/css'));
}

function watchTask() {
    watch(['./src/css/*.css', './src/js/*.js'], { interval: 1000 }, parallel(cssTask, jsTask));
}

exports.default = series(parallel(copyHtml, jsTask, cssTask), watchTask);